Noto Emoji

Emoji fonts (under the fonts subdirectory) are under the SIL Open Font License, version 1.1.
Tools and most image resources are under the Apache license, version 2.0. Flag images under third_party/region-flags are in the public domain or otherwise exempt from copyright (more info).

https://github.com/googlefonts/noto-emoji/blob/master/svg/emoji_u1f41b.svg
