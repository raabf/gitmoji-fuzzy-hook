\>🐛 gitmoji-fuzzy-hook     <a href="https://gitlab.com/raabf/gitmoji-fuzzy-hook"><img src="misc/icon/smaller_bug.png?raw=true" align="right" width="200px"/></a>
=======================

Git commit hook for selecting a [gitm😍ji](https://gitmoji.dev/) from a fuzzy searcher when calling `git commit`,
`git commit -m` or from a GUI-client.

| 🐛   |  ✨  | 🐧 | 🚨 | 🍻 | ✏️  | 🚀 |
| :----: | :----: | :----: | :----: | :----: | :----: | :----: |
| Fixing a bug.| Introducing new features. | Fixing something on Linux. | Critical hotfix. | Writing code drunkenly. | Fixing typos. | Deploying stuff. |

## 📑️ Table of Contents
<!-- MarkdownTOC -->

+ [🧵 Requirements](#requirements)
+ [🏆 Features](#-features)
+ [🎬 Demo](#-demo)
  - [📝 Git commit visual mode \(`git commit`\)](#-git-commit-visual-mode-git-commit)
  - [⌨ Git commit -m \(`git commit -m "My message"`\)](#-git-commit--m-git-commit-m-my-message)
  - [🖥 Git GUI clients](#-git-gui-clients)
+ [🛠 Installation](#-installation)
  - [👔 zsh plugin manager](#-zsh-plugin-manager)
    * [🌺 zplug](#-zplug)
  - [🔨 Manual](#-manual)
+ [🖱 Usage](#-usage)
+ [🔧 Configuration](#-configuration)
+ [#️⃣ Similar Apps](#-similar-apps)

<!-- /MarkdownTOC -->

## 🧵 Requirements

  + git
  + [fzf](https://github.com/junegunn/fzf) (or any other terminal fuzzy finder)
  + [rofi](https://github.com/davatorium/rofi) (or any other GUI fuzzy finder)

## 🏆 Features

  + Fuzzy search for emoji name and description.
  + Auto detects and works with:
      * 📝 Git commit visual mode (`git commit`)
      * ⌨ Git commit message (`git commit -m "My message"`)
      * 🖥 Git GUI clients
  + Auto inserts `🔀` for merges.
  + Available as zsh plugin

## 🎬 Demo

### 📝 Git commit visual mode (`git commit`)
(Pro Tip: Hit <kbd>⇧  </kbd>+<kbd>a</kbd> in vim to directly start typing after the emoji)

![git commit visual gif](misc/demos/git_commit_visual.gif)

### ⌨ Git commit -m (`git commit -m "My message"`)

![git commit -m gif](misc/demos/git_commit_m.gif)

### 🖥 Git GUI clients

![git commit gui gif](misc/demos/git_commit_gui.gif)

## 🛠 Installation

### 👔 zsh plugin manager

The most comfortable method to install it, is with a zsh plugin-manager. This approach has the big advantage that gitmoji-fuzzy-hook CLI will be updated together with your other plugins.

#### 🌺 zplug

    zplug raabf/gitmoji-fuzzy-hook, \
        from:gitlab

It should work also with any other plugin manager. If you have tested it with other plugin managers and if you want to extend this list please tell me at the [issues board](https://gitlab.com/raabf/gitmoji-fuzzy-hook/issues).


### 🔨 Manual

Clone the repo to any location you want:

    git clone https://gitlab.com/raabf/gitmoji-fuzzy-hook.git ~/.local/share/gitmoji-fuzzy-hook

Make a symlink to a folder which is in your `PATH` (Warning it must be a symlink or else `GITMOJI_FUZZY_HOOK_ROOT` must be set):

    ln --force --symbolic ~/.local/share/gitmoji-fuzzy-hook/bin/gitmoji-fuzzy-hook-init.sh $HOME/.local/bin/gitmoji-fuzzy-hook-init

## 🖱 Usage

### Per project activation

To activate the hook in your project execute in your git project root:

    gitmoji-fuzzy-hook-init >> ./.git/hooks/prepare-commit-msg
    chmod +x ./.git/hooks/prepare-commit-msg

### Template activation

You can install it also as a template, which means that every new repository on your machine will automatically get a copy of the required hook.

    mkdir -p "~/.config/git/templates/hooks"
    git config --global init.templatedir "~/.config/git/templates"
    gitmoji-fuzzy-hook-init >> ~/.config/git/templates/hooks/prepare-commit-msg
    chmod +x ~/.config/git/templates/hooks/prepare-commit-msg

### Central activation

With git 2.9+ you can also set a central hook repository for all of your git projects:

    mkdir -p "~/.config/git/hooks"
    git config --global core.hooksPath "~/.config/git/hooks"
    gitmoji-fuzzy-hook-init >> ~/.config/git/hooks/prepare-commit-msg
    chmod +x ~/.config/git/hooks/prepare-commit-msg

### Making commits

After that use normally `git commit`, `git commit -m "my message"`, or the commit dialog of your graphical git client. It should work with any git GUI which calls the git hooks during commit. Most of them call the hook after you specify the message in the dialog, so the gitmoji will appear after you hit the commit button. Note that not all git GUI clients are standard conform and will not call the git hooks.

## 🔧 Configuration

You can configure via various global variables. Either set them in your `.bashrc`/`.zshrc` or modify `prepare-commit-msg` and add them before calling  the gitmoji-fuzzy-hook-exec script.

 + `GITMOJI_FUZZY_HOOK_NO_COMMENTS`: If `true` gitmoji-fuzzy-hook-exec will not add comments to the commit message. Normally it adds a comment in git commit visual mode with the description of the emoji. (Default: `false`)
 + `GITMOJI_FUZZY_HOOK_TERM_CMD`: The fuzzy finder command, which is called when invoked from a terminal. (Default: `fzf`)
 + `GITMOJI_FUZZY_HOOK_GUI_CMD`: The fuzzy finder command, which is called when invoked from a non-terminal (e.g. GUI). (Default: `rofi -dmenu -i`)
 + `GITMOJI_FUZZY_HOOK_ROOT`: The folder where the cloned repo of gitmoji-fuzzy-hook on the local disk. (Default: automatically set when using zsh plugin-manager or manually by symlink, else mendatory)
 + `GITMOJI_FUZZY_HOOK_USE_CODE`: If `true` gitmoji-fuzzy-hook-exec will return the textual code of the emoji instead of the unicode symbol (i.e. `:sparkles:` instead of ✨). If you view your commit in gitlab or github, both will usually render to an emoji symbol (Although, it seems that github renders the texual code to just an png-image, wereas gitlab will render to an unicode symbol), but locally in your console or in an graphical git client, the code `:sparkles:` will usually be displayed as text. You might still prefer the text if you do not want to install a font locally which supports emoji (Default: `false`).
 + `GITMOJI_FUZZY_HOOK_DISABLED`: If `true`, this hook will be deactivated and there is neither an emoji selection nor an emoji will be added to the commit message. Useful if you want to deactivate just this hook and not all hooks, for example in a script (Default: `false`).

## #️⃣ Similar Apps

There is also an overview on [gitmoji.dev/related-tools](https://gitmoji.dev/related-tools).

  + [Gitmoji-commit-hook](https://github.com/tjoskar/gitmoji-commit-hook): Does not work in all modes and has no searching.
  + [Gitmoji-cli](https://github.com/carloscuesta/gitmoji-cli): Is not a hook but a wrapper around the normal git commit.
  + [posh-gitmoji](https://krokofant.github.io/posh-gitmoji/): For powershell instead of bash. Furthermore, it does autocompletion on command instead of fuzzy search.
  + [gitmoji-browser-extension](https://github.com/johannchopin/gitmoji-browser-extension) Browser extension; can search, but copies to clipboard.
