#!/bin/bash
###############################################################################
## Title: gitmoji-fuzzy-hook-exec
## Brief: A prepare-commit-msg hook. Opens a fuzzy searcher with gitmojis, to
##        allow the git commit caller to select one. For git commit visual mode
##        it adds a description of the selected emoji.
## Args:
##      Args from $1 onwards shoud be passed from prepare-commit-msg with $@
##      $1: 0 if this script is called from a terminal
##      $2: File of the commit message
##      $3: Commit type
##      $4: Commit Hash
##
## Returns:
##      The gitmoji (a single character)
##
## Source: https://gitlab.com/raabf/gitmoji-fuzzy-hook
## Author: Fabian Raab <fabian@raab.link>
## Dependencies: awk, fzf, rofi
###############################################################################

is_terminal="$1"
msg_file="$2"
msg_type="$3"
commit_hash="$4"

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

gitmoji_file="$DIR/../data/gitmoji_list.txt"

# fetch the already prepared message
msg="$(cat "$msg_file")"
first_line="$(head -n1 "$msg_file")"

# Set .git location if missing. Hooks are always executed in the git root.
GIT_DIR="${GIT_DIR:-$(pwd)/.git}"

# select default fuzzy finder
TERM_FUZZY=${GITMOJI_FUZZY_HOOK_TERM_CMD:-fzf}
GUI_FUZZY=${GITMOJI_FUZZY_HOOK_GUI_CMD:-rofi -dmenu -i}

# When this emoji is returned then some error happened
emoji="⚠️"
code="ERROR"

if [ "${GITMOJI_FUZZY_HOOK_DISABLED:-false}" == true ]; then
    emoji=""
    code=""

# see https://git-memo.readthedocs.io/en/latest/hooks.html
elif [ "$msg_type" == "merge" ]; then
    emoji="🔀"
    code=":twisted_rightwards_arrows:"

elif [ "$msg_type" == "squash" ] || [ -f "$GIT_DIR/SQUASH_MSG" ] || [ -f "$GIT_DIR/rebase-merge/message-squash" ]; then
# Theoratically "$msg_type" == "squash" should be enough, but in practice
# $msg_type is "commit" even if it is a squash and $GIT_DIR/SQUASH_MSG exists,
# so the existence of the file is also checked.
# In interactive squash-rebase, the other conditions are still not enough, the
# squash message seems to be sometimes in SQUASH_MSG and sometimes in
# rebase-merge/message-squash, I cannot say what is going on there. Either way
# checking both seems to work for now.
    emoji=""
    code=""

elif [ "$msg_type" == "message" ] && [ -f "$GIT_DIR/rebase-merge/git-rebase-todo" ]; then
# Here we are doing a rebase, and during a rebase we use the existing message
# from the history.
    emoji=""
    code=""


elif [ -n "$commit_hash" ]; then  # alternatile: "$msg_type" == "commit"
# $4 is the hash if the commit. If non-empty, the commit is amended and the
# message has already an emoji from the last commit. So we would add a second
# gitmoji without this if.
    emoji=""
    code=""

else
    if [ $is_terminal == 0 ]; then
        # If is_terminal = [ -t 1 ] is true then we were called from terminal
        # (e.g. not gui git client) https://stackoverflow.com/a/4262101/3644818
        line="$(cat "$gitmoji_file" | $TERM_FUZZY)"
    else
        line="$(cat "$gitmoji_file" | $GUI_FUZZY)"
    fi

    emoji="$(echo "$line" | awk '{print $1}')"
    code="$(echo "$line" | awk '{print $2}')"
    description="$(echo "$line" | awk '{$1=$2=""; print $0}')"

    if [ $is_terminal == 0 ] && [ -z "$first_line" ] && [ "$GITMOJI_FUZZY_HOOK_NO_COMMENTS" != true ]; then
        # If is_terminal = [ -t 1 ] is true then we were called from terminal
        # (e.g. not gui git client) https://stackoverflow.com/a/4262101/3644818
        # -z $first_line Checks if we already have a commit message, which
        # happes when invoked with -m, e.g. `git commit -m "commit message"`

        msg="\n# ${emoji} - ${description}${msg}"
        echo -e "$msg" > "$msg_file"
    fi
fi

if [ "${GITMOJI_FUZZY_HOOK_USE_CODE:-false}" == true ]; then
    echo "${code}"
else
    echo "${emoji}"
fi

