#!/bin/bash
###############################################################################
## Title: gitmoji-fuzzy-hook-init
## Brief: Creates a prepare-commit-msg file.
##        It opens a fuzzy searcher with gitmojis, to allow the git commit
##        caller to select one. For git commit visual mode
##        it adds a description of the selected emoji.
## Args:
##      -
##
## Returns:
##      A valid prepare-commit-msg file which calls gitmoji-fuzzy-hook-exec.sh
##
## Source: https://gitlab.com/raabf/gitmoji-fuzzy-hook
## Author: Fabian Raab <fabian@raab.link>
## Dependencies: -
###############################################################################

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

ROOT_BIN=${GITMOJI_FUZZY_HOOK_ROOT:-$DIR}

RED='\033[0;31m'
BLUE='\033[0;34m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color


>&2 echo -e "${RED}>🐛 ${BLUE}gitmoji-fuzzy-hook: Git commit hook for selecting a gitmoji from a fuzzy searcher${NC}"
>&2 echo -e "${BLUE}    USAGE: In a git project folder, execute:${NC}"
>&2 echo -e "${YELLOW}        gitmoji-fuzzy-hook-init >> ./.git/hooks/prepare-commit-msg${NC}"
>&2 echo -e "${YELLOW}        chmod +x ./.git/hooks/prepare-commit-msg${NC}"

cat << EOF
#!/bin/bash
###############################################################################
## Title: gitmoji-fuzzy-hook-init
## Brief: It opens a fuzzy searcher with gitmojis, to allow the git commit
##        caller to select one. For git commit visual mode
##        it adds a description of the selected emoji.
## Args:
##      \$1: File of the commit message
##      \$2: Commit type
##      \$3: Commit Hash
##
## Returns:
##      Prepends a selectable gitmoji to the git commit message.
##
## Source: https://gitlab.com/raabf/gitmoji-fuzzy-hook
## Author: Fabian Raab <fabian@raab.link>
## Dependencies: gitmoji-fuzzy-hook
###############################################################################

[ -t 1 ]  # checks if this script is called from a terminal
emoji="\$(${ROOT_BIN}/gitmoji-fuzzy-hook-exec.sh \$? \$@)"

msg_file="\$1"
msg="\$(cat "\$msg_file")"

# Do here whatever you want with the commit message before prepending the emoji
# to it and writing the message to the commit file.

if [ ! -z "\${emoji}" ]; then  # surpress the space if there is no emoji
	msg="\${emoji} \${msg}"
fi
echo -e "\$msg" > "\$msg_file"

EOF


