#!/bin/zsh

0=${(%):-%N}
GITMOJI_FUZZY_HOOK_ROOT="${0:A:h}"

gitmoji-fuzzy-hook-init()
{
    # `bash -c ...` manipulates $0 so that it contain `gitmoji-fuzzy-hook-init`
    # instead of `gitmoji-fuzzy-hook-init.sh`
    bash -c ". ${GITMOJI_FUZZY_HOOK_ROOT}/bin/gitmoji-fuzzy-hook-init.sh" gitmoji-fuzzy-hook-init ${@}
}

